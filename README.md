# Instalação local

1. Instalar o node.js, que pode ser encontrado na seguinte URL: http://nodejs.org/
2. Rodar `npm install` na pasta do projeto
3. Adicionar o `./node_modules/.bin` no `PATH` do seu sistema operacional
4. Rodar `cake build` para compilar os arquivos
5. Iniciar o servidor utilizando o comando `node build/server.js`

# Versão funcional

A versão funcional do jogo pode ser encontrada na seguinte URL: http://buzzeeling.herokuapp.com/
